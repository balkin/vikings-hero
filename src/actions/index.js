export const increaseSkill = (id) => {
	return {
		type: 'INCREASE_SKILL',
		id: id
	}
};

let skillIdGenerator = 1;

export const newSkill = (name, levels, row, col) => {
	return {
		id: skillIdGenerator++,
		type: "ADD_SKILL",
		levels: levels,
		row: row,
		col: col,
		name: name
	}
};