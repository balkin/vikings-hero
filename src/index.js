import React from "react";
import ReactDOM from "react-dom";
import App from "./App";
import {Provider} from "react-redux";
import "./index.css";
import {createStore} from "redux";
import reducer from "./reducers";
import {newSkill} from "./actions";

const store = createStore(reducer);
store.dispatch(newSkill("Строительство I", 10, 1, 2));
store.dispatch(newSkill("Скорость изучения знаний I", 10, 1, 4));
store.dispatch(newSkill("Увеличение урона I", 5, 2, 1));

ReactDOM.render(
	<Provider store={store}>
		<App />
	</Provider>,
	document.getElementById('root')
);

console.log(store.getState());
