import React, {PropTypes} from "react";

const Skill = ({onClick, id, name, currentLevel, levels}) => (
	<div className="skillIcon" onClick={onClick} style={{backgroundImage: `url(/img/skills/${id}.png)`}}>
		{name}<br />
		{currentLevel} / {levels}
	</div>
);

Skill.propTypes = {
	onClick: PropTypes.func.isRequired,
	name: PropTypes.string.isRequired,
	currentLevel: PropTypes.number.isRequired
};

export default Skill