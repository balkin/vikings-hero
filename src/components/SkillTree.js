import React from "react";
import Skill from "./Skill";
import {connect} from "react-redux";
import {increaseSkill} from "../actions";

const SkillTree = ({skills, onSkillClick}) => (
	<ul>
		{skills.map(skill =>
			<Skill id={skill.id}
			       key={skill.id}
			       name={skill.name}
			       {...skill}
			       onClick={() => onSkillClick(skill.id)}
			/>
		)}
	</ul>
);

const mapStateToProps = (state) => {
	return {
		skills: state.skills.present
	}
};

const mapDispatchToProps = (dispatch) => {
	return {
		onSkillClick: (id) => dispatch(increaseSkill(id))
	}
};

const ConnectedSkillTree = connect(
	mapStateToProps,
	mapDispatchToProps
)(SkillTree);


// TodoList.propTypes = {
// 	todos: PropTypes.arrayOf(PropTypes.shape({
// 		id: PropTypes.number.isRequired,
// 		completed: PropTypes.bool.isRequired,
// 		text: PropTypes.string.isRequired
// 	}).isRequired).isRequired,
// 	onTodoClick: PropTypes.func.isRequired
// }

export default ConnectedSkillTree
