import undoable, {distinctState} from "redux-undo";

const skill = (state, action) => {
	switch (action.type) {
		case 'ADD_SKILL':
			return {
				name: action.name,
				levels: action.levels,
				currentLevel: 0,
				row: action.row,
				col: action.col,
				id: action.id
			};
		case 'INCREASE_SKILL':
			if (state.id !== action.id) {
				return state
			}

			if (state.currentLevel < state.levels) {
				return {
					...state,
					currentLevel: state.currentLevel+1
				}
			}

			return state;

		default:
			return state
	}
};

const skills = (state = [], action) => {
	switch (action.type) {
		case 'ADD_SKILL':
			console.log(...state);
			console.log("skills.ADD_SKILL" + action.id);
			return [
				...state,
				skill(undefined, action)
			];
		case 'INCREASE_SKILL':
			console.log(...state);
			console.log("skills.INCREASE_SKILL " + action.id);
			return state.map(t =>
				skill(t, action)
			);
		default:
			return state
	}
};

const undoableSkills = undoable(skills, {filter: distinctState()});
export default undoableSkills