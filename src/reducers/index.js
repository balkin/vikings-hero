import { combineReducers } from 'redux'
import skills from './skills'
import visibilityFilter from './visibilityFilter'

const todoApp = combineReducers({
	skills,
	visibilityFilter
});

export default todoApp

// export default skills