import React, {Component} from "react";
import logo from "./logo.svg";
import "./App.css";
import SkillTree from "./components/SkillTree";
import UndoRedo from "./containers/UndoRedo";

// http://you-ps.ru/uploads/posts/2013-08/1376601606_1273.png

class App extends Component {
	render() {
		return (
			<div className="App">
				<div className="App-header">
					<img src={logo} className="App-logo" alt="logo"/>
					<h2>Vikings: War of the Clans skill tree calculator</h2>
				</div>
				<SkillTree />
				<UndoRedo />
			</div>
		);
	}
}

export default App;
